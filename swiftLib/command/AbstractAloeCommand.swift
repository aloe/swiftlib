//
//  AbstractAloeCommand.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

class AbstractAloeCommand: NSObject, IAloeCommand {
    var onComplete:AloeCommandCompleteBlock?;
    var onFail:AloeCommandFailBlock?;
    var onProgress:AloeCommandProgressBlock?;
    
    func setCompleteBlock(callback: AloeCommandCompleteBlock) {
        onComplete = callback
    }
    
    func setFailBlock(callback: AloeCommandFailBlock) {
        onFail = callback
    }
    
    func setProgressBlock(callback: AloeCommandProgressBlock) {
        onProgress = callback
    }
    
    func execute() {
        assertionFailure("overrideMe")
    }
    
}
