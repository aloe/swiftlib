//
//  AloeURLLoadOperation.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

protocol AloeURLLoadOperationDelegate{
    func onURLLoadComplete(data:NSData)
    func onURLLoadProgress(progress:Double)
    func onURLLoadFail(error:NSError)
}

class AloeURLLoadOperation: NSOperation,NSURLConnectionDelegate {
    
    var delegate:AloeURLLoadOperationDelegate
    var urlRequest:NSMutableURLRequest?
    var totalBytes:Int64 = 0;
    var loadedBytes:Int64 = 0;
    var connection:NSURLConnection?
    var url:String
    var timeout:NSTimeInterval=5
    var receivedData:NSMutableData
    var isFinished:Bool = false
    var isExecuting:Bool = false
    
    init(url:String, timeout:NSTimeInterval, delegate:
        AloeURLLoadOperationDelegate){
        self.url = url
        self.timeout = timeout
        self.delegate = delegate
        self.receivedData = NSMutableData()
        
        super.init()
    }
    
    override func start() {
        
        urlRequest = NSMutableURLRequest(URL: NSURL(string: url)!)
        urlRequest?.timeoutInterval = timeout
        connection = NSURLConnection(request: urlRequest!, delegate: self, startImmediately: true)
        
        self.setValue(NSNumber(bool:true), forKey: "isExecuting")
        self.setValue(NSNumber(bool:false), forKey:"isFinished")
        while(!self.finished()){
            NSRunLoop.currentRunLoop().runUntilDate(NSDate(timeIntervalSinceNow: 0.2))
        }
    }
    
    private func finished()->(Bool){
        return isFinished
    }
    
    private func executing()->(Bool){
        return isExecuting
    }
    
//    func isConcurrent()->(Bool){
//        return true
//    }
    
    func connection(connection: NSURLConnection, willSendRequest request: NSURLRequest, redirectResponse response: NSURLResponse?) -> NSURLRequest?{
        
        return request
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse){
        totalBytes = response.expectedContentLength
        loadedBytes = 0
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        isExecuting = false
        isFinished = true
        
        delegate.onURLLoadFail(error)
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData){
        receivedData.appendData(data)
        loadedBytes += data.length
        var progress:Double = Double(loadedBytes) / Double(totalBytes);
        delegate.onURLLoadProgress(progress)
        
    }
    
    func connection(connection: NSURLConnection, didSendBodyData bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int){
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection){
        isExecuting = false
        isFinished = true
        delegate.onURLLoadComplete(NSData(data: receivedData))
    }
    
    deinit {
        println("operationDealloc")
    }
}
