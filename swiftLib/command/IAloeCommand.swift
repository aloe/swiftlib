//
//  IAloeCommand.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import Foundation

typealias AloeCommandCompleteBlock = ( data : NSData) -> ()
typealias AloeCommandFailBlock = (error:NSError)->()
typealias AloeCommandProgressBlock = (progress:Double)->()

protocol IAloeCommand {
    
    func execute()->()
    func setCompleteBlock(callback:AloeCommandCompleteBlock)->()
    func setFailBlock(callback:AloeCommandFailBlock)->()
    func setProgressBlock(callback:AloeCommandProgressBlock)->()
}