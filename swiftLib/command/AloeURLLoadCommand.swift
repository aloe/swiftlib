//
//  AloeURLLoadCommand.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

private let MAX_CONNECTION_NUM = 10
private let queue:NSOperationQueue = NSOperationQueue()

class AloeURLLoadCommand: AbstractAloeCommand, AloeURLLoadOperationDelegate{
    
    var url:String
    var timeout:Double = 5.0
    
    init(url_:String){
        url = url_
        timeout = 5
        queue.maxConcurrentOperationCount = MAX_CONNECTION_NUM //一回で良い
    }
    
    init(url_:String, timeout_:Double){
        // どーやんだ
//        AloeURLLoadCommand(url_: url_)
        url = url_
        timeout = timeout_
        queue.maxConcurrentOperationCount = MAX_CONNECTION_NUM //一回で良い
    }
    
    override func execute() {
        var op:AloeURLLoadOperation = AloeURLLoadOperation(url: url, timeout: timeout, delegate: self)
        queue.addOperation(op)
    }
    
    func onURLLoadComplete(data: NSData) {
        if(onComplete != nil){
            onComplete!(data: data)
        }
    }
    func onURLLoadFail(error: NSError) {
        if(onFail != nil){
            onFail!(error: error)
        }
    }
    func onURLLoadProgress(progress: Double) {
        if (onProgress != nil) {
            onProgress!(progress: progress)
        }
    }
    
    deinit{
        println("urlLoadCommandDealloc")
    }
    
}
