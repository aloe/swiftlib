//
//  AloePostCommand.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/16.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

class AloePostCommand: AbstractAloeCommand {
   
    private let params:Dictionary<String, String>
    private let url:String
    
    init(url:String, params:Dictionary<String, String>){
        self.url = url
        self.params = params
    }
    
    override func execute() {
        
        var query:String = "?"
        var keys:[String] = Array(params.keys)
        for key in keys{
            var val:String = params[key]!
            query = query + "&" + key + "=" + AloeUtil.encodeUrl(val)
        }
        
        let queryData:NSData = query.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!
        
        var nsUrl:NSURL = NSURL(string: url)!
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: nsUrl)
        
        request.HTTPMethod = "POST"
        request.HTTPBody = queryData
        
        weak var SELF:AloePostCommand? = self
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            if(self.onComplete != nil){
                self.onComplete?(data: data)
            }
        })
    }
    
    deinit{
        println("postcommand deinit")
    }
    
}
