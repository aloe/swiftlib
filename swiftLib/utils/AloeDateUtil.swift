//
//  AloeDateUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import Foundation

private let formatter:NSDateFormatter = NSDateFormatter()

class AloeDateUtil: NSObject {
    
    class func stringToDate(str:String, format:String)->NSDate{
        formatter.locale = NSLocale(localeIdentifier: "JST")
        formatter.dateFormat = format
        return formatter.dateFromString(str)!
    }
    
    class func dateToString(date:NSDate, format:String)->String{
        formatter.dateFormat = format
        return formatter.stringFromDate(date)
    }
    
}
