//
//  AloeXmlUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/16.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import Foundation

class AloeXmlUtil{
    
    // TODO NSじゃないDictionaryでかえしたい
    class func dataToNSDictionary(data:NSData)->NSDictionary{
        let nsDic:NSDictionary = XMLDictionaryParser.sharedInstance().dictionaryWithData(data)
        return nsDic
    }
    
}
