//
//  AloeDataUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import Foundation

class AloeDataUtil {
    
    class func dataToString(data:NSData)->(String?){
        return NSString(data: data, encoding: NSUTF8StringEncoding)
    }
    
    class func dataToJsonDic(data:NSData)->(NSDictionary?){
        var error : NSError?
        var dic:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as NSDictionary

        if ((error) != nil) {
            println("fail data to json: \(error)")
            return nil
        }
        return dic
    }
    
    class func dataToJsonArray(data:NSData)->(NSArray){
        var error:NSError?
        var array:NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &error) as NSArray
        
        if((error) != nil){
            println("fail data to array: \(error)")
            return NSArray()
        }
        return array
    }
    
    class func strToJson(str:String)->NSDictionary{
        let d:NSData = str.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: false)!
        return dataToJsonDic(d)!
    }
    
}