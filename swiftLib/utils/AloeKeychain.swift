//
//  AloeKeychain.swift
//  cocooru
//
//  Created by kawase yu on 2014/10/21.
//  Copyright (c) 2014年 cocooru. All rights reserved.
//

// http://aska.hatenablog.com/entry/2014/09/23/042116 より

//import UIKit
//import Security
//
//class AloeKeychain {
//    
//    class func save(key: String, data: NSData) -> Bool {
//        let query = [
//            kSecClass       : kSecClassGenericPassword,
//            kSecAttrAccount : key,
//            kSecValueData   : data ]
//        
//        SecItemDelete(query as CFDictionaryRef)
//        
//        let status: OSStatus = SecItemAdd(query as CFDictionaryRef, nil)
//        
//        return status == noErr
//    }
//    
//    class func load(key: String) -> NSData? {
//        let query = [
//            kSecClass       : kSecClassGenericPassword,
//            kSecAttrAccount : key,
//            kSecReturnData  : kCFBooleanTrue,
//            kSecMatchLimit  : kSecMatchLimitOne ]
//        
//        var dataTypeRef :Unmanaged<AnyObject>?
//        
//        let status: OSStatus = SecItemCopyMatching(query, &dataTypeRef)
//        
//        if status == noErr {
//            return (dataTypeRef!.takeRetainedValue() as NSData)
//        } else {
//            return nil
//        }
//    }
//    
//    class func delete(key: String) -> Bool {
//        let query = [
//            kSecClass       : kSecClassGenericPassword,
//            kSecAttrAccount : key ]
//        
//        let status: OSStatus = SecItemDelete(query as CFDictionaryRef)
//        
//        return status == noErr
//    }
//    
//    
//    class func clear() -> Bool {
//        let query = [ kSecClass : kSecClassGenericPassword ]
//        
//        let status: OSStatus = SecItemDelete(query as CFDictionaryRef)
//        
//        return status == noErr
//    }
//    
//}
//
//extension String {
//    public var dataValue: NSData {
//        return dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
//    }
//}
//
//extension NSData {
//    public var stringValue: String {
//        return NSString(data: self, encoding: NSUTF8StringEncoding)!
//    }
//}