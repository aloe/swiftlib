//
//  AloeThreadUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import Foundation

// global?

func mainThread(block: () -> ()) {
    dispatch_async(dispatch_get_main_queue(), block)
}

func subThread(block: () -> ()) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
}

func synced(lock: AnyObject, closure: () -> ()) {
    objc_sync_enter(lock)
    closure()
    objc_sync_exit(lock)
}


class AloeThreadUtil:NSObject{
    
    class func wait(delay:Double, block:()->()){
        subThread { () -> () in
            NSThread.sleepForTimeInterval(delay)
            mainThread({ () -> () in
                block()
            })
        }
    }
    
    class func subThreadBlock(block: () -> ()){
        subThread(block)
    }
    
}