//
//  AloeImageCache.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

private let list:NSMutableDictionary = NSMutableDictionary()

typealias AloeImageCacheCallback = ( UIImage, url:String, useCache:Bool) -> ()
typealias AloeImageCacheFailCallback = ( NSError ) -> ()

class AloeImageCache:NSObject{
    
    class func clear(){
        objc_sync_enter(list)
        list.removeAllObjects()
        objc_sync_exit(list)
    }
    
    private class func getCache(key:String)->(UIImage?){
        objc_sync_enter(list)
        var image:UIImage = list.objectForKey(key) as UIImage
        objc_sync_exit(list)
        return image
    }
    
    private class func setCache(image:UIImage, key:String){
        objc_sync_enter(list)
        list[key] = image
        objc_sync_exit(list)
    }
    
    class func loadImage(url:String, callback:AloeImageCacheCallback, failCallback:AloeImageCacheFailCallback){
        
        subThread { () -> () in
            var image:UIImage? = list.objectForKey(url) as? UIImage
            if(image != nil){
                mainThread({ () -> () in
                    callback(image!, url: url, useCache:true)
                })
                return;
            }
            
            // 毎回は要らない
            var c:AloeURLLoadCommand = AloeURLLoadCommand(url_: url, timeout_: 5.0)
            c.setCompleteBlock({ (data) -> () in
                var image:UIImage? = UIImage(data: data)
                if(image == nil){
                    return
                }
                self.setCache(image!, key: url)
                mainThread({ () -> () in
                    callback(image!, url: url, useCache: false)
                })
            })
            c.setFailBlock({ (error) -> () in
                mainThread({ () -> () in
                    failCallback(error)
                })
            })
            c.execute()
            
        }
    }
    
}