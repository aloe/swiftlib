//
//  AloeFileUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import Foundation

class AloeFileUtil{
    
    class func cachePath()->String{
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        
        return paths[0] as String
    }
    
    class func documentPath()->String{
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        
        return paths[0] as String
    }
    
    class func documentFilePath(fileName:String)->String{
        return self.documentPath() + "/" + fileName
    }
    
    class func cacheDir()->String{
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        
        return paths[0] as String;
    }
    
    class func cacheFilePath(fileName:String)->String{
        return self.cacheDir() + "/" + fileName
    }
    
    class func isExists(path:String)->Bool{
        return NSFileManager().fileExistsAtPath(path)
    }
    
    class func mkDir(dirPath:String){
        if(self.isExists(dirPath)){
            return
        }
        
        var error:NSError?
        
        NSFileManager.defaultManager().createDirectoryAtPath(dirPath, withIntermediateDirectories: true, attributes: nil, error: &error)
        if(error != nil){
            println("fail: \(error)")
        }else{
            println("mkDir.created \(dirPath)")
        }
    }
    
    class func tmpPath()->String{
        return NSTemporaryDirectory()
    }
    
    class func tmpFilePath(fileName:String)->String{
        return self.tmpPath() + "/" + fileName
    }
    
}
