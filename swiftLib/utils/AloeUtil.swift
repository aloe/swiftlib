//
//  AloeUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit
import CoreTelephony

private var jaDevice:Bool = true
private var activeAppCountKey:String = "activeAppCountKey"

class AloeUtil: NSObject{
    
    class func setup(){
        if(UIDevice.currentDevice().model.hasSuffix("Simulator")){
            jaDevice = false
            return
        }
        let netinfo:CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
        let carrier:CTCarrier = netinfo.subscriberCellularProvider
        let countryCode:NSString = carrier.mobileCountryCode;
        jaDevice = (countryCode == "440" || countryCode == "441");
    }
    
    class func openBrowser(url:String){
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }
    
    class func pain(view:UIView){
        let fromScale:CGFloat = 0.95
        view.transform = CGAffineTransformMakeScale(fromScale, fromScale)
        AloeTween.doTween(0.2, ease: AloeEase.OutBounce, { (val) -> () in
            let scale:CGFloat = fromScale + ((1.0-fromScale) * val)
            view.transform = CGAffineTransformMakeScale(scale, scale)
        })
    }
    
    class func addSkipBackupAttributeToItemAtURL(url:NSURL)->Bool{
        let path:String = url.path!
        let filePath = path.fileSystemRepresentation()
        
        var attrName = "com.apple.MobileBackup";
        
        var anInt: Int = 1
        var anIntSize: Int = sizeof(anInt.dynamicType)
        var result = setxattr(filePath, attrName, &anInt, UInt(anIntSize), 0, 0);
        return result == 0;
    }
    
    class func epochSeconds()->UInt64{
        return UInt64(floor(CFAbsoluteTimeGetCurrent() + kCFAbsoluteTimeIntervalSince1970))
    }
    
    class func epochMilliSeconds()->UInt64{
        return self.epochSeconds() * 1000;
    }
    
    class func encodeUrl(str:String)->String{
        return str.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
    }
    
    class func tryShowReview(title:String, message:String, appId:String){
        
    }
    
    class func activeApp(){
        var activeCount:Int = self.activeAppCount()
        activeCount++
        let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        ud.setInteger(activeCount, forKey: activeAppCountKey)
        ud.synchronize()
    }
    
    class func activeAppCount()->Int{
        let c:Int = NSUserDefaults.standardUserDefaults().integerForKey(activeAppCountKey)
        return c
    }
    
    class func isJaDevice()->Bool{
        return jaDevice
    }
    
}


