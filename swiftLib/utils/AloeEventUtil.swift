//
//  AloeEventUtil.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class AloeEventUtil: NSObject {
   
    
    class func addGlobalEventListener(target:AnyObject, selector:Selector, name:String){
        NSNotificationCenter.defaultCenter().addObserver(target, selector: selector, name: name, object: nil)
    }
    
    class func dispatchGlobalEvent(name:String){
        let notification:NSNotification = NSNotification(name: name, object: nil)
        NSNotificationCenter.defaultCenter().postNotification(notification)
    }

}
