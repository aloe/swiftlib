//
//  AloeDeviceUtil.swift
//  swiftLib
//
//  Created by kawase yu on 2014/09/11.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

class AloeDeviceUtil: NSObject {

    class func is35Inch()->Bool{
        return (self.windowHeight() < 568)
    }
    
    class func windowHeight()->CGFloat{
        return UIScreen.mainScreen().bounds.size.height
    }
    
    class func windowWidth()->CGFloat{
        return UIScreen.mainScreen().bounds.size.width
    }
    
    class func windowFrame()->CGRect{
        return UIScreen.mainScreen().bounds
    }
    
    class func osVersion()->Float{
        return (UIDevice.currentDevice().systemVersion as NSString).floatValue
    }
    
}
